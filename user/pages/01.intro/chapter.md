---
title: Introduciton
taxonomy:
    category: docs
---

### Introduction

# Hesper framework

Hesper is a logical continuation of industrial-grade php framework OnPHP, 
bringing this excellent software to modern standards.

Hesper and its parent are used in many industries' top-notch companies, and has 
proven its reliability and ability to handle most difficult tasks, ranging from
highly-complex business logic to severe traffic handling.

## Why hesper?  

The heart of framework - original object composition and control system allows 
the following:

- Automatic object and method creating - allowing blazingly fast prototyping
- Very flexible ORM, originated from Java's Hibernate backport to PHP
- Automatic database structure support
- Automatic migrations
- Most expansive support of best open-source database - PostgreSQL, including 
JSON/JSONB support, window functions and other contemporary features
- Somewhat limited MySQL, MS SQL and Oracle support
- PSR4 and namespace support

## Limitations

- Skill requirements are somewhat high, engineer working with Hesper is expected 
to have experience in object-oriented programming 
- Your PHP interpreter version must support namespaces and traits, 
PHP7 is preferred.
- Some features are supported only by PostgreSQL driver, due to being not impleented 
in other RDBMS features
- Some features, like implementation of web application core, user handling, 
authorization etc are miniscule - we await you to write them yourself, or use 
one of the publicly available plugins.  
- Hesper is originated from time-proven codebase, and its rewrite to modern 
standards is work in progress. You may find a couple of WTFs in depths of code
- if you do so, please send us a message or submit pull request. 

